public with sharing class IdeaThemeController {
    
    public List<SelectOption> zoneList{get;set;}
    public List<SelectOption> themeStatusList{get;set;}
    public static List<IdeaTheme> themeListObj {get;set;}
    public static String message{get;set;}
    public static List<Idea> ideaList{get;set;}
    public static List<IdeaTheme> campaignList{get;set;}
    public static List<IdeaComment> ideaCommentList{get;set;}
    public static IdeaTheme campaignObj{get;set;}
    
    IdeaThemecontroller(){
        
        //getThemeDetails();

    }
    
    @AuraEnabled
    public static List<IdeaTheme> getThemeDetails(){
        themeListObj = [SELECT Id,Title FROM IdeaTheme where Status=:'New'];
        //ideaThemeObj = [SELECT Status FROM IdeaTheme];
        return themeListObj;
    }
    
    @AuraEnabled
    public static String getUserName() {
        System.debug('Username----------->'+userinfo.getName());
        return userinfo.getName();
    }
    
    @AuraEnabled
    public static String getUserProfile(){
        Profile p=[Select Name from Profile where Id=:userinfo.getProfileId() limit 1];
        return p.Name;
    }
    
    @AuraEnabled
    public static Integer getIdeasCountVsUser(){
        Integer numOfIdeas=[SELECT count() FROM Idea WHERE CreatedById =: userinfo.getUserId()];
        return numOfIdeas;
    }
    
    /*@AuraEnabled
    public static List<Idea> getAllIdeas(){
        ideaList=[Select Id,Title,Body,VoteTotal,CreatorName,CreatedDate,VoteCount__c from Idea ORDER BY CreatedDate DESC limit 5];
        System.debug('Idea check-->'+ideaList[0]);
        return ideaList;
    }*/
    
    @AuraEnabled
    public static List<IdeaTheme> getAllCampaigns(){
        campaignList=[SELECT CommunityId,Description,EndDate,Id,LeaderboardId__c,StartDate,Status,Title,Zone_Id__c FROM IdeaTheme where CommunityId=:'09a28000000DRZeAAO' limit 5];
        //System.debug('cam check-->'+campaignList[0]);
        return campaignList;
    }
    
    @AuraEnabled
    public static List<Idea> getLatestIdeaByCampaign(Id campaignId){
        //campaignId='0Bg28000000CaX1';
        if(campaignId!=null){
        	ideaList=[Select Id,Title,Body,VoteTotal,CreatorName,CreatedDate,VoteCount__c from Idea where IdeaThemeId=:campaignId ORDER BY CreatedDate DESC limit 5];
        }
        else{
            ideaList=[Select Id,Title,Body,VoteTotal,CreatorName,CreatedDate,VoteCount__c from Idea ORDER BY CreatedDate DESC limit 5];
        }
        //System.debug('Idea check-->'+ideaList[0]);
        return ideaList;
    }
    /*@AuraEnabled
    public static List<Idea> getPopularIdea(){
        
        ideaList=[Select Id,Title,Body,VoteTotal,CreatorName,CreatedDate,VoteCount__c from Idea ORDER BY VoteTotal DESC limit 5];
        
        return ideaList;
    }*/
    
    @AuraEnabled
    public static void postComment(Id ideaId,String body){
        IdeaComment commentObj=new IdeaComment();
        commentObj.IdeaId=ideaId;
        //commentObj.UpVotes=voteCount;
        commentObj.CommentBody=body;
        System.debug('commentObj------>'+commentObj);
        try{
        	insert commentObj;
        }
        catch(Exception e){
            System.debug('Exception--->'+e);
        }
    }
    
    @AuraEnabled
    public static List<IdeaComment> getAllCommentsByIdeaId(Id ideaId){
        ideaCommentList = [SELECT CommentBody,CommunityId,CreatorName,CreatedDate,IdeaId,UpVotes FROM IdeaComment where IdeaId=:ideaId ORDER BY CreatedDate desc limit 500];
        System.debug('Idea comment-->'+ideaCommentList);
        return ideaCommentList;
    }
    
    @AuraEnabled
    public static IdeaTheme getCampaignDetails(Id campaignId){
        campaignObj=[SELECT CommunityId,Description,EndDate,Id,LeaderboardId__c,StartDate,Status,Title,Zone_Id__c FROM IdeaTheme where Id=:campaignId limit 1];
        
        return campaignObj;
    }
    
    @AuraEnabled
    public static List<Idea> getPopularIdeaByCampaign(Id campaignId){
        System.debug('-------->'+campaignId);
        if(campaignId!=null){
        	ideaList=[Select Id,Title,Body,VoteTotal,CreatorName,CreatedDate,VoteCount__c,IdeaThemeId from Idea where IdeaThemeId=:campaignId ORDER BY VoteCount__c DESC limit 5];
        }
        else{
            ideaList=[Select Id,Title,Body,VoteTotal,CreatorName,CreatedDate,VoteCount__c,IdeaThemeId from Idea ORDER BY VoteTotal DESC limit 5];
        }
        return ideaList;
    }
    
    @AuraEnabled
    public static List<String> getThemeDetails1(){
        /*ideaThemeObj = [SELECT Categories,CommunityId,CreatedById,CreatedDate,Description,EndDate,Id,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,StartDate,Status,SystemModstamp,Title FROM IdeaTheme];
        
        return ideaThemeObj;*/
        Schema.SObjectType phoneObjType = Schema.getGlobalDescribe().get('IdeaTheme');//From the Object Api name retrieving the SObject
        Sobject Object_name = phoneObjType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get('Status').getDescribe().getPickListValues();
        List<String> ideaThemeStatus=new List<String>();
        if(pick_list_values!=null){
            
        for (Schema.PicklistEntry pickListVal : pick_list_values) { //for all values in the picklist list
          ideaThemeStatus.add(pickListVal.getValue());//add the value  to our final list
        }
        }
        return ideaThemeStatus;
    }
    
    @AuraEnabled
    public static Idea getIdeaDetails(Id ideaId){
        String voterStr; 
        Boolean flag = true;
        Idea ideaObj=[Select id,Title,Body,VoteTotal,VoteCount__c,Voters__c from Idea where id=:ideaId];
        if(ideaObj != null && ideaObj.Voters__c != null){
            voterStr = ideaObj.Voters__c;
            if((ideaObj.Voters__c).contains(UserInfo.getUserId()))
            {
                flag = false;
            }
        }
        ideaObj.Voters__c = ''+flag;
        system.debug('ideaObj ==> '+ideaObj);
        return ideaObj;
        
    }
    
    @AuraEnabled
    public static void promoteAnIdea(Id ideaId){
        Idea ideaObj=[Select id,Title,Body,VoteTotal,VoteCount__c,Voters__c from Idea where id=: ideaId];
        ideaObj.VoteCount__c=ideaObj.VoteCount__c+10;
        system.debug(ideaId +'<--  UserInfo.getUserId() --> '+UserInfo.getUserId());
        if(ideaObj != null && (ideaObj.Voters__c != null && ideaObj.Voters__c !='') ){
        	ideaObj.Voters__c = ideaObj.Voters__c+'|'+UserInfo.getUserId();    
        }else{
        	ideaObj.Voters__c = UserInfo.getUserId();    
        }
        upsert ideaObj;
    }
    
    
    
        
    @AuraEnabled
    public static Id submitIdea(id idIdeaTheme,string Status,string Body,string Categories,string Title){
        IdeaTheme objItdeaT = [select id from IdeaTheme where id =:idIdeaTheme];
        Idea objIdea = new Idea();
        if(objItdeaT!=null){
            objIdea.IdeaThemeId = objItdeaT.id;}
       // objIdea.Status = Status;
        if(Body!=null&& Body!=''){
            objIdea.Body = Body;}
        if(Title!=null&& Title!=''){
            objIdea.Title = Title;}
        if(Categories!=null){
            objIdea.CommunityId=Categories;}
        
        objIdea.Voters__c = UserInfo.getUserId(); 
        
        insert objIdea;
        return objIdea.Id;        
    }
    @AuraEnabled
    public static void cancelIdea(){
        
    }
    
    
}