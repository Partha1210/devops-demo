public class Ideathem_ligt{

    @AuraEnabled
    public static List<IdeaTheme> getIdeaTheam(){
        List<IdeaTheme> lst= [Select Id,Description ,Title From IdeaTheme limit 2];
        return lst;
    }


    @AuraEnabled
    public static List<IdeaTheme> getAllIdeaTheam(){
        List<IdeaTheme> lst= [Select Id,Description ,Title From IdeaTheme];
        return lst;
    }
       @AuraEnabled
    public static Id postIdea(id idIdeaTheme,string Status,string Body,string Categories,string Title){
        IdeaTheme objItdeaT = [select id from IdeaTheme where id =:idIdeaTheme];
        Idea objIdea = new Idea();
        
        objIdea.IdeaTheme = objItdeaT;
        objIdea.Status = Status;
        objIdea.Body = Body;
        objIdea.Categories =Categories;
        objIdea.Title = Title;
        
        insert objIdea;
        return objIdea.Id;
    }


}