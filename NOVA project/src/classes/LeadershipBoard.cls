public class LeadershipBoard {
    
    @AuraEnabled
    public static List<LeadershipBoard__c> getLeadershipBoard() {
        
        List<LeadershipBoard__c> rankList = [SELECT Rank__c, CreatorName__c, CreatorPhoto__c, Points__c 
                                             FROM LeadershipBoard__c];
        
        return rankList;
    }   
}