public with sharing class LeadershipBoardController {
    
    @AuraEnabled
    public static List<IdeaTheme> getIdeaTheme() {
        List<IdeaTheme> themeList = [SELECT Title FROM IdeaTheme ORDER BY Title];       
        return themeList;
    }
    
    @AuraEnabled
    public static List<Rank__c> getLeadershipBoard() {       
        List<Rank__c> rankList = [SELECT IdeaTheme__r.Title, Rank__c, PostedBy__c, CreatorPhoto__c, Points__c  
                                   FROM Rank__c ORDER BY IdeaTheme__r.Title, Rank__c];
        return rankList;
    }
}