public with sharing class OrganizationRoleController{

    public OrganizationRoleController() {
        getRoleUserJson();
    }


    /********************* Properties used by getRootNodeOfUserTree function - starts **********************/
    // map to hold roles with Id as the key
    public static Map <Id, UserRole> roleUsersMap;
    public static String jsonStr{get; set;}     


    /*******************************************************************
    Purpose: This constructor will be called from VF page
    Parameters: ApexPages.StandardController
    Returns: 
    Throws [Exceptions]: 
    ********************************************************************/
    public OrganizationRoleController(ApexPages.StandardController controller) {
        getRoleUserJson();
    }
    
    
    
    public static List<UserRole> getRoleUser() {
        List<Id> IdList;
       // Get role to users mapping in a map with key as role id
        roleUsersMap = new Map<Id, UserRole>([select Id, Name, parentRoleId, (select id, name from users) from UserRole order by parentRoleId]);

       // IdList =  new List<Id>(roleUsersMap.keySet());
       
     //  getRoleUserJson();
       
        return roleUsersMap.values();
    }
    
    @AuraEnabled
    public static String getRoleUserJson(){
        List<UserRole> URLst;
        jsonStr = '';
        // Get role to users mapping in a map with key as role id
        roleUsersMap = new Map<Id, UserRole>([select Id, Name, parentRoleId, (select id, name from users) from UserRole order by parentRoleId]);
        URLst =  new List<UserRole>(roleUsersMap.values());
        
        system.JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject(); // {
        gen.writeFieldName('core');
        gen.writeStartObject(); // {
        gen.writeFieldName('data');
        gen.writeStartArray(); // [

        for(UserRole ur : URLst){
            gen.writeStartObject(); // {  
            gen.writeIdField('id',ur.id);
            if(ur.parentRoleId == null){
                gen.writeStringField('parent','#');
            }
            else{
                gen.writeStringField('parent', ur.parentRoleId);
            }   
            gen.writeStringField('text', ur.Name);

            gen.writeEndObject(); // }          
        }
        gen.writeEndArray(); // ]
        gen.writeEndObject(); // }
        gen.writeEndObject(); // }
        
        jsonStr = gen.getAsString();
        jsonStr = jsonStr.replace('\r\n', ' ');
        jsonStr = jsonStr.replace('\n', ' ');
        jsonStr = jsonStr.replace('\r', ' ');

        system.debug('jsonStr ----> ' + jsonStr);
        
        return jsonStr;
        
    }

 
}