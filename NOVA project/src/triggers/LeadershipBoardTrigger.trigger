trigger LeadershipBoardTrigger on Idea (after insert, after update, after delete) {

    List<LeadershipBoard__c> rankList = new List<LeadershipBoard__c>();
    List<AggregateResult> ideaList = new List<AggregateResult>();
    Integer rank = 0;
    rankList = [ SELECT Id FROM LeadershipBoard__c ];
    DELETE rankList;
    
    ideaList = [ SELECT CreatedById, CreatorSmallPhotoUrl, 
                Creator_Name__c, Count(Title) Total FROM Idea 
                Group By CreatedById, Creator_Name__c, CreatorSmallPhotoUrl 
                ORDER BY Count(Title) DESC ];
    
    for(AggregateResult ar : ideaList) {
        LeadershipBoard__c LeadershipBoard = new LeadershipBoard__c();
        LeadershipBoard.Rank__c = ++rank;
        LeadershipBoard.CreatorPhoto__c = (String)ar.get('CreatorSmallPhotoUrl');
        LeadershipBoard.CreatorName__c = (String)ar.get('Creator_Name__c');
        LeadershipBoard.Points__c = (Integer)ar.get('Total') * 10;
        
        insert LeadershipBoard;
    }
}