trigger RankTrigger on Idea (after insert, after update, after delete) {
   
    List<Rank__c> rankList = new List<Rank__c>();
    List<IdeaTheme> themeList = new List<IdeaTheme>();
    List<AggregateResult> idea = new List<AggregateResult>();
    Integer num = 0;
    
    rankList = [ SELECT ID FROM Rank__c ];
    DELETE rankList;
   
    themeList = [ SELECT Title FROM IdeaTheme ];
  
    for (IdeaTheme theme : themeList) {     
            
        idea = [ SELECT CreatedById, Creator_Name__c, CreatorSmallPhotoUrl, 
                Count(Title) Total, IdeaTheme.Id Theme FROM Idea 
                WHERE IdeaTheme.Title=: theme.Title
                GROUP BY CreatedById, Creator_Name__c, CreatorSmallPhotoUrl, IdeaTheme.Id
                ORDER BY Count(Title) DESC ]; 
                      
        num = 0;
                
        for(AggregateResult ar : idea) {
            Rank__c rank = new Rank__c();
            rank.IdeaTheme__c =  (String)ar.get('Theme');
            rank.Rank__c = ++num;
            rank.PostedBy__c = (String)ar.get('Creator_Name__c');
            rank.CreatorPhoto__c = (String)ar.get('CreatorSmallPhotoUrl');
            rank.Points__c = (Integer)ar.get('Total') * 10;
                        
            insert rank;             
        }
    }
}